Papaver Somniferum [POPPY]
===================


![logo](https://cdn.pbrd.co/images/GC8SRoM.png)





[TOC]


----------

*Papaver Somniferum, or POPPY,  is initially an x11 hybrid until it reaches height 40,000. It is pure PoS from height 40,001.*

**PoS is active from block 1.**

----------


PapaverSomniferum-qt
-------------

[Wallet-QT](https://bitbucket.org/TRUMPINSANE/poppy-qt/downloads/)

-----

PapaverSomniferum Explorer
-------------

[Explorer](http://explorer.soporific.info:3001)


-----


Website
-------------

[soporific.info](http://soporific.info)

-----



POW Rewards
-------------

Mined by proof-of work, the following details the Papaver Somniferum reward structure:

> 
> - **NB:** 
> - **The reward changes every 4,000 blocks**
> - **Block spacing is 1 minute**
> - **Difficulty changes every 10 minutes**
> - **POW difficulty drops quickly when no blocks are being mined**
> 

-



Maximum Height | POW Reward (coins per block)
----  | ---
  250 | 1   (1 coin reward up till block 250)
 4000 | 150 (150 coins reward up till block 4,000)
 8000 | 92  (92 reward till block 8,000) ...
12000 | 105
16000 | 99
20000 | 93
24000 | 125
28000 | 200
32000 | 125
36000 | 110
40000 | 500







POS Rewards
-------------

- 8% per year
- Minimum staking: 1 hour
- Maximum staking: unlimited
- POS starts at block 1
- 25 mining confirmations
- Premine 1.5 million block 1


-----


General coin information
-------------


- Coin: **PapaverSomniferum**
- Ticker: **POPPY**
- Address prefix: **8**
- Algorithm: **x11 Hybrid / pure PoS from 40,000 blocks** 
- RPC Port: **8155**
- P2P Port: **8154**


-----

Splash
-------------

![Splash screen](https://cdn.pbrd.co/images/GBwNO2i.png)


------

Exchanges
-------------

I will pay a **0.10 BTC ADDCOIN** listing at **Yobit** exchange. So there will be no canvasing for votes, etc.

**It usually takes the exchange anything from a few hours to a month to add submitted coins. However, officially, the site states 4 - 7 business days.**

------


Copy Paste to make life easy
-------------

The coin has a long name, which is Latin, and hard to spell.

Officially it is:

> 
> - **PapaverSomniferum**
> 


-

If you plan to run a daemon, unomp or just even want a .conf file, copy paste it in as a command/name or you will pull your hair out.

Depending on how you compile, run a daemon, unomp or use a .conf file, you will want one of these:


>
> - **PapaverSomniferum**
> 
> - **./PapaverSomniferumd**
>
> - **PapaverSomniferumrpc**
>
> - **PapaverSomniferum.conf**
>
> - **/root/.PapaverSomniferum/PapaverSomniferum.conf**
> 
> - **papaversomniferum.json**
> 
> - **Papaversomniferum**
>


-----


Blockhash, TX and address for API for blokexplorer (block 2)
-------------

- 00000f4c4b098948044fbd62deda8aea7c69fa915e587c8de47d20bd7b1ed102
- 33abec5906133cdb0eabedc0e0d56d001f4df48e326406035da9976e3307585b
- 8USq7Dr4sKTkFNDmqbm8M92LwCe84h8nVb

-----

Genesis and txhash (block 0)
-------------

- 00000351bdd3da4c435aa1aebe6586af5f3bb990399be9f07598f39e12ecf4ef
- 5e9da022aa074a962def9e363da5fd61cdf229ce483564bafb48b9d02653afaa

-----


Wallet bkg
------


![bkg](https://cdn.pbrd.co/images/GBwOMq7.png)


------



Compiled using QT4 deps
-------------

Avoid QT5

